module git.pmx.cn/tq/kit

go 1.16

require (
	github.com/go-kit/kit v0.12.0
	github.com/go-kit/log v0.2.1
	github.com/nacos-group/nacos-sdk-go/v2 v2.1.3
)
