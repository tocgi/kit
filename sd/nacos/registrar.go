package nacos

import (
	"fmt"

	"github.com/nacos-group/nacos-sdk-go/v2/vo"

	"github.com/go-kit/log"
)

type Registrar struct {
	client         IClient
	registration   *vo.RegisterInstanceParam
	deregistration *vo.DeregisterInstanceParam
	logger         log.Logger
}

func NewRegistrar(client IClient, r *vo.RegisterInstanceParam, d *vo.DeregisterInstanceParam, logger log.Logger) *Registrar {

	return &Registrar{
		client:         client,
		registration:   r,
		deregistration: d,
		logger:         log.With(logger, "service", r.ServiceName, "Metadata", fmt.Sprint(r.Metadata), "address", r.Ip),
	}

}

func (p *Registrar) Register() {
	if _, err := p.client.Register(*p.registration); err != nil {
		p.logger.Log("err", err)
	} else {
		p.logger.Log("action", "register")
	}
}

func (p *Registrar) Deregister() {
	if _, err := p.client.Deregister(*p.deregistration); err != nil {
		p.logger.Log("err", err)
	} else {
		p.logger.Log("action", "deregister")
	}
}
